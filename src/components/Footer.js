import React from "react"

const Footer = () => {
	return(
		<div style={footer}>
			<h3>@Iwan Setiyo. 2021</h3>
		</div>
		)
}

export default Footer;

const footer = {
	display: "flex",
	background: "#fe0240",
	justifyContent: "center",
	color: "#fff",
	alignItems: "center",
	position: "absolute",
	bottom: "0",
	width: "100%"

}