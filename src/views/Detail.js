import React from 'react'
import { Link } from "react-router-dom"
import MainLayout from "../layout/MainLayout"

class Detail extends React.Component {
	state = {
		skills: [
			{
				id: 1,
				name: "Javascript",
				description: "Javascript Desc",
				image: require("../images/javascript.png").default
			},
			{
				id: 2,
				name: "React Js",
				description: "ReactJS Desc",
				image: require("../images/react.jpg").default
			},
			{
				id: 3,
				name: "Vue JS",
				description: "Vue Desc",
				image: require("../images/vuejs.jpg").default
			}
		],
		data: ""
	}

	componentDidMount(){
		const id = this.props.match.params.id
		const data = this.state.skills.find(item => item.id === parseInt(id) )

		this.setState({data})
	}

	render(){
		const { data } = this.state
		console.log(data)
		return(
			<MainLayout>
				<div style={container}>
					<h1>{data.name}</h1>
					<div style={box}>
						<img style={img} src={data.image} alt={data.image}/>
						<div>
							<p>{data.description}</p>
							<Link to='/'>back home</Link>
						</div>
					</div>
				</div>
			</MainLayout>
			)
	}
}

export default Detail;

const container = {
	padding: "0 6rem"
}

const img = {
	width: "15rem",
	height: "20rem",
	marginRight: "2rem"
}

const box = {
	display: "flex"
}