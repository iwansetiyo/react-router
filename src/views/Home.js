import React from 'react'
import { Link } from "react-router-dom"
import MainLayout from "../layout/MainLayout"

class Home extends React.Component {
	state = {
		skills: [
			{
				id: 1,
				name: "Javascript",
				description: "Javascript Desc",
				image: require("../images/javascript.png")
			},
			{
				id: 2,
				name: "React Js",
				description: "ReactJS Desc",
				image: require("../images/react.jpg")
			},
			{
				id: 3,
				name: "Vue JS",
				description: "Vue Desc",
				image: require("../images/vuejs.jpg")
			}
		]
	}
	render() {
		const { skills } = this.state
		console.log(skills)
		return(
			<MainLayout>
				<h1 style={{paddingLeft: "6rem"}}>Home</h1>
				<div style={container}>
					{skills.map(item =>
					<div key={item.id} style={card}>
						<Link to={`/detail/${item.id}`}>
							<img src={item.image.default} alt={item.name} style={img}/>
							<h3 style={{textAlign: "center"}}>{item.name}</h3>
						</Link>
					</div>
					)}
				</div>
			</MainLayout>
			)
	}
}

export default Home;

const container = {
	display: "flex",
	padding: "0 6rem",
	justifyContent: "space-between"
}

const card = {
	width: "20%",
	height: "15rem"
}

const img = {
	width: "100%",
	height: "100%"
}