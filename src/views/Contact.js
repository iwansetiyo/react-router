import React from "react"
import MainLayout from "../layout/MainLayout"

const Contact = () => {
	return(
		<MainLayout>
			<div style={container}>
				<h1>Contact Us</h1>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
					Morbi eget neque eu nisl rhoncus posuere non quis lectus. 
					Nam id maximus est, et ornare lectus. Ut ipsum arcu, elementum sed turpis sit amet, v
					olutpat blandit mi. Donec quis congue ligula. Nam commodo ornare mauris, eu blandit eros faucibus nec. 
					Morbi ornare ac mi nec euismod. Morbi blandit neque sodales facilisis congue. 
					Nunc nisi ligula, posuere et ultrices nec, tempus sit amet orci. Mauris vitae turpis vel erat ullamcorper porttitor. 
					Suspendisse elementum blandit velit. Donec venenatis pulvinar sapien at rutrum.
				</p>
			</div>
		</MainLayout>
	)
}

export default Contact;

const container = {
  padding: "0 6rem"
}